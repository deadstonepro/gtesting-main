from django.urls import path

from . import views


urlpatterns = [
        path("problem", views.problem_view, name="problem"),
        path("problems", views.problems_view, name="problems"),
        path("submition/<int:pk>", views.submition_view, name="submition"),
        path("submit/<int:problem_id>", views.submit_view, name="submit"),
        path("_report/<int:submition_id>", views.report_view, name="report"),
]
