from django import forms

class ProblemForm(forms.Form):

    alias = forms.CharField(max_length=64)
    data = forms.CharField(widget=forms.Textarea)


class SubmitForm(forms.Form):

    language = forms.ChoiceField(choices=(("python3", "python3"),))
    source = forms.CharField(widget=forms.Textarea)
