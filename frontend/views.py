import json
from typing import Dict

from django.http import Http404, HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.conf import settings
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
import requests
import gtesting_server_shared as shared

from .forms import ProblemForm, SubmitForm
from .models import Problem, Submition, TestReport


def upload_testet(data: Dict) -> str:
    upload_url = f"{settings.GTESTING_SERVER_HOST}/testset"
    resp = requests.post(upload_url, json=data)
    testset = shared.TestSet.Schema().load(resp.json())
    return testset._id


def problem_view(request):
    if request.method == "GET":
        form = ProblemForm()
        return render(request, "frontend/upload_problem.html", { "form": form })

    if request.method == "POST":
        form = ProblemForm(request.POST)
        if form.is_valid():
            testset_obj = json.loads(form.cleaned_data["data"])
            testset_id = upload_testet(testset_obj)
            problem = Problem(alias=form.cleaned_data["alias"], testset_id=testset_id)
            problem.save()
            return redirect("problems")

    raise Http404("Method not allowed")


def problems_view(request):
    if request.method == "GET":
        problems = Problem.objects.all()
        return render(request, "frontend/problems.html", { "problems": problems })

    raise Http404("Method not allowed")


def submit_view(request, problem_id):
    problem = get_object_or_404(Problem, pk=problem_id)
    if request.method == "GET":
        form = SubmitForm()
        return render(request, "frontend/submit.html", { "form": form, "problem": problem })

    if request.method == "POST":
        form = SubmitForm(request.POST)
        if not form.is_valid():
            raise NotImplementedError()
        submition = Submition(
            problem=problem,
            language=form.cleaned_data["language"],
            source=form.cleaned_data["source"])
        submition.save()
        rel_url = reverse("report", kwargs={"submition_id": str(submition.pk)})
        callback_url = request.build_absolute_uri(rel_url)
        submit_request = {
            "source": submition.source,
            "language": submition.language,
            "testset_id": problem.testset_id,
            "callback_url": callback_url,
        }
        submit_url = f"{settings.GTESTING_SERVER_HOST}/submit"
        resp = requests.post(submit_url, json=submit_request)
        return redirect("submition", pk=submition.pk)


def submition_view(request, pk):
    submition = get_object_or_404(Submition, pk=pk)
    tests = submition.testreport_set.all()
    return render(request, "frontend/submition.html", context={
        "submition": submition,
        "tests": tests})


@csrf_exempt
def report_view(request, submition_id):
    data = json.loads(request.read())
    report = shared.SubmitionReport.Schema().load(data)
    submition = get_object_or_404(Submition, pk=submition_id)
    submition.status = Submition.Status.from_gtesting_shared(report.status)
    for _test_report in report.reports[submition.tests_passed:len(report.reports)]:
        test_report = TestReport(
            submition=submition,
            verdict=TestReport.Verdict.from_gtesting_shared(_test_report.verdict))
        test_report.save()
    submition.tests_passed = len(report.reports)
    submition.save()
    return HttpResponse(status=200)
