from django.utils.translation import gettext_lazy as _
from django.db import models

import gtesting_server_shared as shared
from gtesting_server_shared import TestReport, SubmitionReport


class Problem(models.Model):

    alias = models.CharField(max_length=128, unique=True)
    testset_id = models.CharField(max_length=128)


class Submition(models.Model):

    class Status(models.TextChoices):
        WAITING             = "Waiting", _("Waiting")
        COMPILATION         = "Compilation", _("Compilation")
        COMPILATION_FAILED  = "Compilation Failed", _("Compilation Failed")
        RUNNING             = "Running", _("Running")
        FAILED              = "Failed", _("Failed")
        FINISHED            = "Finished", _("Finished")

        @classmethod
        def from_gtesting_shared(cls, status: shared.SubmitionReport.Status):
            """ Compatible with gtesting-server-shared==0.0.4 """
            mapping = {
                shared.SubmitionReport.Status.WAITING: cls.WAITING,
                shared.SubmitionReport.Status.COMPILATION: cls.COMPILATION,
                shared.SubmitionReport.Status.COMPILATION_FAILED: cls.COMPILATION_FAILED,
                shared.SubmitionReport.Status.RUNNING: cls.RUNNING,
                shared.SubmitionReport.Status.FAILED: cls.FAILED,
                shared.SubmitionReport.Status.FINISHED: cls.FINISHED,
            }
            return mapping[status]

    problem = models.ForeignKey(Problem, on_delete=models.CASCADE)
    source = models.CharField(max_length=65536)
    language = models.CharField(max_length=128)
    status = models.CharField(max_length=64, choices=Status.choices, default=Status.WAITING)
    tests_passed = models.IntegerField(default=0)


class TestReport(models.Model):

    class Verdict(models.TextChoices):
        OK = "OK", _('Success')
        RE = "RE", _('RuntimeError')
        WA = "WA", _('Wrong Answer')

        @classmethod
        def from_gtesting_shared(cls, shared_report: shared.TestReport):
            """ Compatible with gtesting-server-shared==0.0.4 """
            mapping = {
                shared.TestReport.Verdict.OK: cls.OK,
                shared.TestReport.Verdict.WA: cls.WA,
                shared.TestReport.Verdict.RE: cls.RE,
            }
            return mapping[shared_report]

    submition = models.ForeignKey(Submition, on_delete=models.CASCADE)
    message = models.CharField(max_length=65536)
    verdict = models.CharField(max_length=2, choices=Verdict.choices)
